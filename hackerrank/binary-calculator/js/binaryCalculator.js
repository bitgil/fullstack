let resEl;
let btnsEl;
let operand1 = '';
let operand2 = '';
let operator = '';

function init() {
    const containerEL = document.createElement('div');
    containerEL.classList.add('flex-container');
    document.body.appendChild(containerEL);

    const rowEl = document.createElement('div');
    rowEl.classList.add('row');
    containerEL.appendChild(rowEl);

    resEl = document.createElement('div');
    resEl.id = 'res';
    rowEl.appendChild(resEl);

    btnsEl = document.createElement('div');
    btnsEl.id = 'btns';
    rowEl.appendChild(btnsEl);

    const btn0 = document.createElement('button');
    btn0.id = 'btn0';
    btn0.classList.add('btn');
    btn0.innerHTML = 0;
    btnsEl.appendChild(btn0);

    const btn1 = document.createElement('button');
    btn1.id = 'btn1';
    btn1.classList.add('btn');
    btn1.innerHTML = 1;
    btnsEl.appendChild(btn1);

    const btnClr = document.createElement('button');
    btnClr.id = 'btnClr';
    btnClr.classList.add('btn');
    btnClr.innerHTML = 'C';
    btnsEl.appendChild(btnClr);

    const btnEql = document.createElement('button');
    btnEql.id = 'btnEql';
    btnEql.classList.add('btn');
    btnEql.innerHTML = '=';
    btnsEl.appendChild(btnEql);

    const btnSum = document.createElement('button');
    btnSum.id = 'btnSum';
    btnSum.classList.add('btn');
    btnSum.innerHTML = '+';
    btnsEl.appendChild(btnSum);

    const btnSub = document.createElement('button');
    btnSub.id = 'btnSub';
    btnSub.classList.add('btn');
    btnSub.innerHTML = '-';
    btnsEl.appendChild(btnSub);

    const btnMul = document.createElement('button');
    btnMul.id = 'btnMul';
    btnMul.classList.add('btn');
    btnMul.innerHTML = '*';
    btnsEl.appendChild(btnMul);

    const btnDiv = document.createElement('button');
    btnDiv.id = 'btnDiv';
    btnDiv.classList.add('btn');
    btnDiv.innerHTML = '/';
    btnsEl.appendChild(btnDiv);

    btnsEl.addEventListener('click', function(event) {
        processInput(event);
    })
}

function processInput(event) {
    const target = event.target;
    console.log(target.id, ' was pressed');
    if (target.id === 'btnClr') {
        clear();
    } else if (target.id === 'btnEql') {
        setOperands(resEl.innerHTML);
        console.log(operand1, operator, operand2);
        let result = Math.trunc(eval(operand1 + operator + operand2));
        resEl.innerHTML = result.toString(2);
    } else if (target.id === 'btn0' || target.id === 'btn1') {
        resEl.innerHTML += target.innerHTML;
    } else {
        operator = target.innerHTML;
        console.log(operator);
        resEl.innerHTML += target.innerHTML;
    }
}
function clear() {
    resEl.innerHTML = '';
    operand1 = '';
    operand2 = '';
    operator = '';
}
function setOperands(inputString) {
    let operands = inputString.split(operator);
    if (operands.length === 2) {
        operand1 = parseInt(operands[0], 2);
        operand2 = parseInt(operands[1], 2);
        console.log('op1=', operand1, ', op2=', operand2, ', op=', operator);
    } else {
        console.error('you have not entered two operands');
    }
}
