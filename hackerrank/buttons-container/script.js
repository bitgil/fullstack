function initButtons() {
	const containerEl = document.createElement('div');
	containerEl.id='container';
	containerEl.classList.add('flex-container');
	document.body.appendChild(containerEl);

	const rowEl = document.createElement('div');
	rowEl.classList.add('row');
	containerEl.appendChild(rowEl);

	for (let i = 0; i < 9; i++) {
		const btnEl = document.createElement('button');
		btnEl.id = 'btn' + i;
		btnEl.className = 'btns';  // also btnEl.classList.add('btns');
		btnEl.innerHTML = i + 1;
		btnEl.style.width = '30%';
		btnEl.style.height = '75px';
		btnEl.style.fontSize = '2em';

		rowEl.appendChild(btnEl);
	}

	const btn5 = document.getElementById('btn4');
	btn5.addEventListener('click', function() {
		rotate();
	})
}

const offsets = [3, -1, -1, 3, 0, -3, 1, 1, -3];
let newLabels = [];

function rotate() {
	const btns = document.getElementsByTagName('button');
	const btnArray = Array.from(btns);

	for (const [i, btn] of btnArray.entries()) {
		newLabels[i] = btnArray[i + offsets[i]].innerHTML;
	}
	console.log('new labes', newLabels);
	for (const [i, btn] of btnArray.entries()) {
		btn.innerHTML = newLabels[i];
	}
}