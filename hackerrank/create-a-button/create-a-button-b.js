function btnClick() {
  const divEl = document.createElement('div');
  document.body.appendChild(divEl);

  const btnEl = document.createElement('button');
  btnEl.id = 'btn';
  btnEl.innerHTML = '0';
  divEl.appendChild(btnEl);

  btnEl.addEventListener('click', function() {
    let count = btnEl.innerHTML;
    btnEl.innerHTML = Number(count) + 1;
  });
}